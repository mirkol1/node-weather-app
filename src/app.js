const express = require('express');
const path = require('path');
const hbs = require('hbs');
//const request = require('request');
const geocode = require('./utils/geocode');
const forecast = require('./utils/forecast');

const app = express();
const port = process.env.PORT || 3000;
const publicDirectoryPath = path.join(__dirname, '../public'); 
const viewsPath = path.join(__dirname, '../templates/views');
const partialsPath = path.join(__dirname, '../templates/partials');

app.set('view engine', 'hbs');
app.set('views', viewsPath);
hbs.registerPartials(partialsPath);

app.use(express.static(publicDirectoryPath));
app.get('', (req, res) => {
    res.render('index', {
        title: 'Weather App',
        name: 'Mirko Luznjanin'
    })
})
app.get('/about', (req, res) => {
    res.render('about', {
        title: 'About',
        name: 'Mirko Luznjanin'
    })
})
app.get('/help', (req, res) => {
    res.render('help', {
        title: 'Help page',
        name: 'Mirko Luznjanin',
        text: 'temp help text paragraph'
    })
})
app.get('/weather', (req, res) => {
    if (!req.query.address) {
        return res.send({
            error: 'You must provide location!'
        })
    }
    
    geocode(req.query.address, (error, { latitude, longitude, location} = {}) => {
        if (error) {
            return res.send({ error });
        }
        forecast(latitude, longitude, (error, forecastData) => {
            if (error) {
                return res.send({ error });
            }
            res.send({
                forecast: forecastData,
                location,
                address: req.query.address
            })
        });
    });    
})

 
app.get('/products', (req,res) => {
    if (!req.query.search) {
        return res.send({
            error: 'You must provide search term!'
        })
    } 
    console.log(req.query.search);
    res.send({
        products: []
    })
})

app.get('/help/*', (req, res) => {
    res.render('404', {
        title: 404,
        name: 'Mirko Luznjanin',
        errorMessage: 'Article not found'
    })
});

app.get('*', (req, res) => {
    res.render('404', {
        title: 404,
        name: 'Mirko Luznjanin',
        errorMessage: 'Page not found'
    })
});

app.listen(port, () => {
     console.log('Server is up on '+ port);
});