const request = require('request');

const forecast = (latitude, longitude, callback) => {
    const url = 'http://api.weatherstack.com/current?access_key=e8bb6d8bcc8a9d331c9a2c490b2f1400&query='+ latitude +','+ longitude;
    
    request({ url, json: true}, (error, { body }) => {
        //console.log(response.body.features);
        if (error) {
            callback('Unable to connect to weather service!', undefined)
        } else if (body.error) {
            //console.log(response.body)
            callback('Unable to find location, try another search.', undefined)
        } else {
            callback(undefined, 'It is currently ' + body.current.temperature + ' degrees out. There is ' + body.current.precip + '% chance of rain.');
        }
    })
}

module.exports = forecast;