const weatherForm = document.querySelector('form');
const search = document.querySelector('input');
const messageOne = document.querySelector('#p1');
const messageTwo = document.querySelector('#p2');



weatherForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const location = search.value;
    messageOne.textContent = '';
    messageTwo.textContent = '';

    fetch('/weather?address=' + location).then((response) => {
    response.json().then((tempData) => {
        if(tempData.error) {
            messageOne.textContent = tempData.error;
        } else {
            messageOne.textContent = tempData.location;
            messageTwo.textContent = tempData.forecast;
        }
    })
})
})